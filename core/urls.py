"""core URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from app_crud_1 import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('home/', views.home),
    path('familles/', views.ViewFamilles),
    path('familles/ajouter/famille/', views.ViewAjouterFamille),
    path('familles/ajouter/famille/done/', views.ViewFamilleAjoute),
    path('familles/medicaments/<str:id>/', views.ViewMedicaments),
    path('familles/medicaments/<str:id>/détails/<str:idM>/', views.ViewDetailMed),
    path('familles/supprimer/famille/<str:id>/', views.ViewSupprimerFamille),
    path('familles/ajouter/medicament/<str:id>/', views.ViewAjouterMed),
    path('familles/ajouter/medicament/<str:id>/done/', views.ViewMedAjouter),
    path('familles/medicaments/<str:id>/supprimer/<str:idM>', views.ViewSupprimerMed),
    path('familles/medicaments/<str:id>/modifier/', views.ViewModifier),
    path('familles/medicaments/<str:id>/modifier/done/', views.ViewModifierDone)
]
