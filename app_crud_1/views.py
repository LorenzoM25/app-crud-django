from django.shortcuts import render, get_object_or_404, get_list_or_404
from django.http import HttpResponse
from app_crud_1.models import Famille, Medicament

def home(request):
    return render(request, 'home.html')

def ViewFamilles(request):
    familles = Famille.objects.all()
    #form = FamilleForm()
    return render(request, 'familles.html', {'familles': familles})

def ViewMedicaments(request, id):
    medicaments = get_list_or_404(Medicament, idFamille=id)
    len(medicaments)
    return render(request, 'medicaments.html', {'medicaments': medicaments})

def ViewAjouterFamille(request):
    return render(request, 'AjouterFamille.html')

def ViewFamilleAjoute(request):
    newIdF = request.POST.get('newIdF')
    newLibelle = request.POST.get('newLibelle')
    newFamille = Famille.objects.create(idF=newIdF, libelle= newLibelle)
    return render(request, 'FamilleAjouter.html')

def ViewSupprimerFamille(request, id):
    famille = get_object_or_404(Famille, id = id)
    famille.delete()
    return render(request, 'familleSupprimer.html')

def ViewDetailMed(request, id, idM):
    detailsMedicaments = get_object_or_404(Medicament, idM=idM)
    return render(request, 'detailMedicament.html', {'detailMed': detailsMedicaments})

def ViewAjouterMed(request, id):
    return render(request, 'AjouterMed.html', {'idFamille': id})

def ViewMedAjouter(request, id):
    newIdM = request.POST.get('idM')
    newNomCommercial = request.POST.get('nomCommercial')
    newidF = get_object_or_404(Famille, id = id)
    newComposition = request.POST.get('composition')
    newEffets = request.POST.get('effets')
    newContreIndications = request.POST.get('contreIndications')
    
    newMed = Medicament.objects.create(idM = newIdM, nomCommercial = newNomCommercial, idFamille = newidF, composition = newComposition, effets = newEffets, contreIndications = newContreIndications)
    
    return render(request, 'MedAjoute.html')


def ViewSupprimerMed(request, id, idM):
    medicament = get_object_or_404(Medicament, idM = idM)
    medicament.delete()
    return render(request, 'MedSupprimer.html')

def ViewModifier(request, id):
    medicament = get_object_or_404(Medicament, idM = id)
    return render(request, 'ModifierMedicament.html', {'medicament': medicament})

def ViewModifierDone(request, id):
    newIdM = request.POST.get('idM')
    newNomCommercial = request.POST.get('nomCommercial')
    newComposition = request.POST.get('composition')
    newEffets = request.POST.get('effets')
    newContreIndications = request.POST.get('contreIndications')
    
    
    medicament = get_object_or_404(Medicament, idM = id) 
    medicament.idM = newIdM
    medicament.nomCommercial = newNomCommercial
    medicament.composition = newComposition
    medicament.effets = newEffets
    medicament.contreIndications = newContreIndications
    medicament.save()
    
    return render(request, 'MedModifier.html')