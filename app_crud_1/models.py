from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

class Famille(models.Model):
    idF = models.fields.CharField(max_length=3)
    libelle = models.fields.CharField(max_length=100)
    slug = models.fields.SlugField(max_length=3)
    def __str__(self):
        return f'{self.idF}'


class Medicament(models.Model):
    idM = models.fields.CharField(max_length=30)
    nomCommercial = models.fields.CharField(max_length=30)
    idFamille = models.ForeignKey(Famille, null=True, on_delete=models.SET_NULL)
    composition = models.fields.CharField(max_length=100)
    effets = models.fields.CharField(max_length=250)
    contreIndications = models.fields.CharField(max_length=250)
    def __str__(self):
        return f'{self.idM}'