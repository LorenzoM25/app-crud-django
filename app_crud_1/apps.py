from django.apps import AppConfig


class AppCrud1Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'app_crud_1'
