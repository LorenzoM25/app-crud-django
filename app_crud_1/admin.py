from django.contrib import admin
from app_crud_1.models import Famille
from app_crud_1.models import Medicament

class FamilleAdmin(admin.ModelAdmin):
    list_display = ('id', 'idF', 'libelle', 'slug')
    
class MedicamentAdmin(admin.ModelAdmin):
    list_display = ('id', 'idM', 'nomCommercial', 'idFamille', 'composition', 'effets', 'contreIndications')

admin.site.register(Famille, FamilleAdmin)
admin.site.register(Medicament, MedicamentAdmin)